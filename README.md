# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |

---

### How to use 

![](brush.jpg) 
**Tool Not Selected** <br>
![](brushPressed.jpg)
**Tool Selected**

### Functions

![](upload.jpg)<br>
#### Upload
Upload image to canvs

![](download.jpg)<br>
#### Download
Download the image you draw

![](undo.jpg)<br>
#### Undo
Undo the step

![](redo.jpg)<br>
#### Redo
Redo the step

![](reset.jpg)<br>
#### Reset
Reset the canvas

![](brushStyle.jpg)<br>
#### Brush Style
Change Brush Color and Size
    
![](brush.jpg)<br>
#### Brush
Brush your canvas

![](eraser.jpg)<br>
#### Eraser
Erase your paint

![](line.jpg)<br>
#### Line
To make Straight Line

![](circle.jpg)<br>
#### Circle
To make a Circle

![](triangle.jpg)<br>
#### Triangle
To make a Triangle

![](rectangle.jpg)<br>
#### Rectangle
To make a Rectangle

![](text.jpg)<br>
#### Text
Allows to type Text on the canvas

![](fontStyle.jpg)<br>
#### Text Style
Change Text Size and Font Style


### Gitlab page link

    https://106010018.gitlab.io/AS_01_WebCanvas

<style>
table th{
    width: 100%;
}
</style>